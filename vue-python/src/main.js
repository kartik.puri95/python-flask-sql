import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'

let baseURL

if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
  baseURL = 'http://127.0.0.1:5000'
} else {
  baseURL = 'http://api.example.com'
}

export const axiosinstance = axios.create(
  {
    baseURL: baseURL
  })
new Vue({
  el: '#app',
  render: h => h(App)
})
